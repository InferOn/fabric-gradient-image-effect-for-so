//(function(global) {
//
//      'use strict';
//
//      var fabric = global.fabric || (global.fabric = {}),
//        extend = fabric.util.object.extend;
//
//      fabric.Image.filters.ImagePatternEffect = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
//
//        type: 'GradientEffect',
//
//        initialize: function (options) {
//          options = options || {};
//          this.gradient = options.gradient || {};
//          this.img = options.img;
//        },
//
//        applyTo: function(canvasEl) {
//          var gr = this.gradient;
//          var w = this.img._element.naturalWidth;
//          var h = this.img._element.naturalHeight;
//          var hc = document.createElement('canvas');
//
//          hc.setAttribute('width', w);
//          hc.setAttribute('height', h);
//
//          var fhc = new fabric.Canvas(hc);
//          var rect = new fabric.Rect({
//            fill: 'transparent',
//            width: w,
//            height: h
//          });
//
//          rect.setGradient('fill', gr);
//
//          fhc.add(rect);
//          fhc.renderAll();
//
//          var ifhcContext = fhc.getContext('2d');
//          var fhcImageData = ifhcContext.getImageData(0, 0, fhc.width, fhc.height);
//          var fhcData = fhcImageData.data;
//
//          var context = canvasEl.getContext('2d'),
//            imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
//            data = imageData.data;
//
//          for (var i = 0, n = data.length; i < n; i += 4) {
//            if(data[i] != 0 && data[i+1] != 0 && data[i+2] != 0) {
//              data[i] += fhcData[i];
//              data[i + 1] += fhcData[i + 1];
//              data[i + 2] += fhcData[i + 2];
//
//            }
//          }
//
//          context.putImageData(imageData, 0, 0);
//        },
//
//        toObject: function() {
//          return extend(this.callSuper('toObject'), {
//            gradient: this.gradient
//
//          });
//        }
//      });
      
      (function(global) {

      'use strict';

      var fabric = global.fabric || (global.fabric = {}),
        extend = fabric.util.object.extend;

      fabric.Image.filters.ImagePatternEffect = fabric.util.createClass(fabric.Image.filters.BaseFilter, {

        type: 'ImagePatternEffect',

        initialize: function (options) {
          options = options || {};
          
          this.img = options.img;
        },

        applyTo: function(canvasEl) {
          
          var w = canvasEl.width;
          var h = canvasEl.height;
          var hc = document.createElement('canvas');
          
          fabric.Image.fromURL('images/smile.png', function(smile) {
            debugger;
              hc.setAttribute('width', w);
              hc.setAttribute('height', h);
    
              var fhc = new fabric.StaticCanvas(hc);
              fhc.add(smile);
              var pattern = new fabric.Pattern({
                source: function() {
                  fhc.setDimensions({
                    width: smile.getWidth() + 5,
                    height:smile.getHeight() + 5,
                  });
                  return fhc.getElement();
                },
                repeat: 'repeat'
              });
              var rect = new fabric.Rect({
                fill: pattern,
                width: w,
                height: h
              });
              
              fhc.add(rect);
              fhc.renderAll();
    
              var ifhcContext = fhc.getContext('2d');
              var fhcImageData = ifhcContext.getImageData(0, 0, fhc.width, fhc.height);
              var fhcData = fhcImageData.data;
    
              var context = canvasEl.getContext('2d'),
                imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
                data = imageData.data;
    
              for (var i = 0, n = data.length; i < n; i += 4) {
                  data[i] += fhcData[i];
                  data[i + 1] += fhcData[i + 1];
                  data[i + 2] += fhcData[i + 2];
    
              }
    
              context.putImageData(imageData, 0, 0);
            
          });
          
        },

        toObject: function() {
          return extend(this.callSuper('toObject'), {
            

          });
        }
      });

      fabric.Image.filters.ImagePatternEffect.fromObject = function(object) {
        return new fabric.Image.filters.ImagePatternEffect(object);
      };

    })(typeof exports !== 'undefined' ? exports : this);


var canvas = new fabric.Canvas('c');


fabric.Image.fromURL('images/tshirt.png', function(img) {
  var orImg =img;
  img.filters.push(new fabric.Image.filters.ImagePatternEffect({
    
    img: orImg,
    
  }));
  img.applyFilters(canvas.renderAll.bind(canvas));
  canvas.add(img.set({
    width: 300,
    height: 300,

  }));
}, {
  crossOrigin: ''
});

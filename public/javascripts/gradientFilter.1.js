//(function(global) {
//
//      'use strict';
//
//      var fabric = global.fabric || (global.fabric = {}),
//        extend = fabric.util.object.extend;
//
//      fabric.Image.filters.ImagePatternEffect = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
//
//        type: 'GradientEffect',
//
//        initialize: function (options) {
//          options = options || {};
//          this.gradient = options.gradient || {};
//          this.img = options.img;
//        },
//
//        applyTo: function(canvasEl) {
//          var gr = this.gradient;
//          var w = this.img._element.naturalWidth;
//          var h = this.img._element.naturalHeight;
//          var hc = document.createElement('canvas');
//
//          hc.setAttribute('width', w);
//          hc.setAttribute('height', h);
//
//          var fhc = new fabric.Canvas(hc);
//          var rect = new fabric.Rect({
//            fill: 'transparent',
//            width: w,
//            height: h
//          });
//
//          rect.setGradient('fill', gr);
//
//          fhc.add(rect);
//          fhc.renderAll();
//
//          var ifhcContext = fhc.getContext('2d');
//          var fhcImageData = ifhcContext.getImageData(0, 0, fhc.width, fhc.height);
//          var fhcData = fhcImageData.data;
//
//          var context = canvasEl.getContext('2d'),
//            imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
//            data = imageData.data;
//
//          for (var i = 0, n = data.length; i < n; i += 4) {
//            if(data[i] != 0 && data[i+1] != 0 && data[i+2] != 0) {
//              data[i] += fhcData[i];
//              data[i + 1] += fhcData[i + 1];
//              data[i + 2] += fhcData[i + 2];
//
//            }
//          }
//
//          context.putImageData(imageData, 0, 0);
//        },
//
//        toObject: function() {
//          return extend(this.callSuper('toObject'), {
//            gradient: this.gradient
//
//          });
//        }
//      });
      
      (function(global) {

      'use strict';

      var fabric = global.fabric || (global.fabric = {}),
        extend = fabric.util.object.extend;

      fabric.Image.filters.ImagePatternEffect = fabric.util.createClass(fabric.Image.filters.BaseFilter, {
        type: 'ImagePatternEffect',
        initialize: function (options) {
          options = options || {};
          this.imgPattern = options.imgPattern;
          
        },
        applyTo: function(canvasEl) {
          var context = canvasEl.getContext('2d');
          var patternSourceCanvas = new fabric.StaticCanvas();
          
          var ii = this.imgPattern;
          ii.scaleToWidth(25);
          patternSourceCanvas.add(this.imgPattern);
          var pattern = new fabric.Pattern({
            source: function() {
              var ww = ii.width;
              var hh = ii.heigh;
//              debugger;
              patternSourceCanvas.setDimensions({
                width: ww,
                height:hh,
              });
              return patternSourceCanvas.getElement();
            },
            repeat: 'repeat'
          });
          //debugger;
//          
          patternSourceCanvas.add(new fabric.Rect(
          {
            width: canvasEl.width,
            height: canvasEl.height,
            fill: pattern
          }));
          
          var ifhcContext = patternSourceCanvas.getContext('2d');
          var fhcImageData = ifhcContext.getImageData(0, 0, patternSourceCanvas.width, patternSourceCanvas.height);
          var fhcData = fhcImageData.data;

          var imageData = context.getImageData(0, 0, canvasEl.width, canvasEl.height),
            data = imageData.data;

          for (var i = 0, n = data.length; i < n; i += 4) {
              data[i] += fhcData[i];
              data[i + 1] += fhcData[i + 1];
              data[i + 2] += fhcData[i + 2];

          }
          context.putImageData(imageData, 0, 0);
        },

        toObject: function() {
          return extend(this.callSuper('toObject'), {

          });
        }
      });

      fabric.Image.filters.ImagePatternEffect.fromObject = function(object) {
        return new fabric.Image.filters.ImagePatternEffect(object);
      };

    })(typeof exports !== 'undefined' ? exports : this);


var canvas = new fabric.Canvas('c');
var imgOutside;
fabric.Image.fromURL('images/smile.png', function(img) {
  debugger;
  imgOutside = img;
  fabric.Image.fromURL('images/tshirt.png', function(innerImg) {
    debugger;
    
    innerImg.filters.push(new fabric.Image.filters.ImagePatternEffect({
    imgPattern: imgOutside,
    
    }));
    innerImg.applyFilters(canvas.renderAll.bind(canvas));
    canvas.add(innerImg.set({
      width: 300,
      height: 300,
  
    }));
  }, {
    crossOrigin: ''
  });
  
}, {
  crossOrigin: ''
});

